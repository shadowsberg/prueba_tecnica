<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use DB;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            'uuid'       => Str::uuid(),
            'expires_at' => '2019-01-01',
            'status'     => 'pending',
            'client_id'  => 1,
            'clp_usd'    => 810
        ]);

        DB::table('payments')->insert([
            'uuid'         => Str::uuid(),
            'payment_date' => '2019-12-01',
            'expires_at'   => '2020-01-01',
            'status'       => 'paid',
            'client_id'    => 1,
            'clp_usd'      => 820
        ]);
    }
}
