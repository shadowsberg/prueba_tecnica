<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB; 

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'email'     => 'admin@example.com',
            'join_date' => '2022-10-18'
        ]);
    }
}
