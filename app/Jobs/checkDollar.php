<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;

class checkDollar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, SerializesModels;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client   = new Client(['base_uri' => 'https://mindicador.cl']);
        $response = $client->request('GET', '/api');
        $result   = json_decode($response->getBody());
        return   $result->dolar->valor;
    }


}
