<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Events\PaymentEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Jobs\checkDollar;
use App\Models\Payment;
use App\Models\Client;
use Carbon\Carbon;


class ApiController extends Controller
{
    //Method to list clients
    public function clients(){
        $clients = Client::all();
        return response()->json($clients);
    }

    //Method to list the payments of a specific client
    public function payments(Request $request){
        $payments = Payment::where('client_id', $request->client)->get();
        return response()->json($payments);
    }

    public function payment(Request $request){
        $payment = Payment::whereDate('payment_date', Carbon::today())->first();
        if(is_null($payment)){
            $dollar = checkDollar::dispatchSync();
        }else{
            $dollar = $payment->clp_usd;
        }
        $payment               = new Payment;
        $payment->uuid         = Str::uuid();
        $payment->payment_date = Carbon::now();
        $payment->expires_at   = Carbon::now()->addMonth();
        $payment->status       = $request->status;
        $payment->client_id    = $request->client_id;
        $payment->clp_usd      = $dollar;
        $client = Client::where('id', $request->client_id)->first();
        $email = $client->email;
        if($payment->save()){
            event(new PaymentEvent($email));
            return response()->json($payment);
        }else{
            return response()->json(['error' => 'The payment could not be registered'], 404);
        }
    }
}
